<?php
    include "includes/header.php";
?>

<body>
    <?php
        include "includes/navigation.php";
    ?>

    <header id="main-header" class="py-2 bg-primary text-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <h1 class="text-center">
                        <i class="fa fa-truck"></i> Violation Details</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- PROFILE EDIT -->

    <section id="profile">
        <div class="container">
            <div class="row justify-content-center">
                <div class=" col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="text-center">violation details</h4>
                        </div>
                        <div class="card-body">
                            <form>
                            <div class="form-group">
                                <label for="numberplate">NumberPlate</label>
                                <input type="text" value="UEA" name="numberplate" class="form-control" disabled>
                            </div>
                            
                            <div class="form-group">
                                <label for="file">Truck Image</label><br>
                                <img height="100" width="200" src="trucks/truck2.jpg" alt="Truck Image">
                                
                            </div>
                            <div class="form-group">
                                <label for="driverName">Driver's Name</label>
                                <input type="text" value="Onen Felix" name="driverName" class="form-control" disabled>
                            </div>
                            <div class="form-group">
                                <label for="driverContact">Driver's Contact</label>
                                <input type="text" value="+256785450481" name="driverContact" class="form-control" disabled>
                            </div>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 bg-primary">
                    <div class="card">
                            <div class="card-header">
                                <h4 class="text-center">Verify Clearence</h4>
                            </div>
                            <div class="card-body">
                            <form>
                                <div class="form-group">
                                    <label for="numberplate"><strong>Cleared By:</strong> </label> Felix Onen <br>
                                    <div class="row">
                                        <div class="col">
                                            <h3>Your Profile Image</h3>
                                            <img src="img/avatar.png" alt="" height="150" width="150"  class="d-block img-fluid ">
                                        </div>
                                    </div>
                                    <img height="150" width="150" src="trucks/truck2.jpg" alt="Truck Image"><br>
                                    <label for="numberplate"><strong>Violation Date:</strong> 22-3-2020 14:00 </label>
                                    <input type="text" value="1" name="clearedby" class="form-control" hidden><br>
                                    <input type="text" value="1" name="cleared" class="form-control" hidden><br>
                                    <button type="submit" class="btn mx-auto d-block btn-primary">Verify</button>
                                </div>
                            </form>

                            </div>
                    </div> 

                </div>
                
            </div>
        </div>
    </section>

    <footer id="main-footer" class="bg-dark text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">Copyright &copy; 2020 WebMonitor </p>
                </div>
            </div>
        </div>
    </footer>


    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>