<?php
    include "includes/header.php";
?>

<body>
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark p-0">
        <div class="container">
            <a href="index.html" class="navbar-brand">WEB ALCOHOL DUI MONITOR</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">

            </div>
        </div>
    </nav>

    <header id="main-header" class="py-2 bg-primary text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        <i class="fa fa-user"></i>Traffic Officer Login Portal</h1>
                </div>
            </div>
        </div>
    </header>
    <!--ACTION-->
    <section id="action" class="py-4 mb-4 bg-light">
        <div class="container">
            <div class="row">
            </div>
        </div>
    </section>
    <!-- LOGIN -->

<?php
    // If form submitted, insert values into the database.
    if (isset($_POST['username'])){
            // removes backslashes
        $username = stripslashes($_REQUEST['username']);
            //escapes special characters in a string
        $username = mysqli_real_escape_string($con,$username);
        $password = stripslashes($_REQUEST['password']);
        $password = mysqli_real_escape_string($con,$password);
        //Checking is user existing in the database or not
            $query = "SELECT * FROM `officers` WHERE username='$username'
    and password='$password'";
        $result = mysqli_query($con,$query) or die(mysql_error());
        $rows = mysqli_num_rows($result);
        $data = mysqli_fetch_assoc($result);
        $_SESSION['userid'] = $data['id'];
            if($rows==1){
            $_SESSION['username'] = $username;
            
                // Redirect user to index.php
            header("Location:".$url);
            }else{
        echo "<div class='form'>
    <h3>Username/password is incorrect.</h3>
    <br/>Click here to <a href='index.php'>Login</a></div>";
        }
        }else{
?>

    <section id="login">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h4>Account Login</h4>
                        </div>
                        <div class="card-body">
                            <form action="index.php" method="POST">
                                <div class="form-group">
                                    <label for="text">Username</label>
                                    <input type="text" name="username" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" class="form-control" required>
                                </div>
                                <input type="submit" class="btn btn-primary btn-block" value="Login">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php } ?>

    <footer id="main-footer" class="bg-dark fixed-bottom text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">Copyright &copy; 2020 WebMonitor </p>
                </div>
            </div>
        </div>
    </footer>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>