<?php
    include "includes/header.php";
    require('../db.php');
?>

<body>
    <?php
        include "includes/navigation.php";
    ?>

    <header id="main-header" class="py-2 bg-primary text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        <i class="fa fa-gear"></i>Registered Officers 
                    </h1>
                </div>
            </div>
        </div>
    </header>
    <!--ACTION-->
            
    <!-- POST -->

    <!-- pagination -->
    <?php
        //get current pagenumber
        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        //The formula for php pagination

        $no_of_records_per_page = 10;
        $offset = ($pageno-1) * $no_of_records_per_page; 
        // Get the number of total number of pages
        $total_pages_sql = "SELECT COUNT(*) FROM officers";
        $result = mysqli_query($con,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);
        //Constructing the SQL Query for pagination and retrieving information
        $sql = "SELECT * FROM officers LIMIT $offset, $no_of_records_per_page"; 
        $result = mysqli_query($con,$sql);
        // $rows = mysqli_fetch_array($result)
    ?>

    <section id="posts">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h4>Traffic Officers</h4>
                        </div>
                        <div class="table-responsive-lg">
                            <table class="table table-striped">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Phone Number</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 1;
                                    while($rows = mysqli_fetch_assoc($result)){
                                        $id = $rows['id'];
                                        $firstname = $rows['firstname'];
                                        $lastname = $rows['lastname'];
                                        $phonenumber = $rows['phonenumber'];

                                    

                                    ?>
                                    <tr>
                                        <td scope="row"><?php echo $count; ?></td>
                                        <td><?php echo $firstname; ?></td>
                                        <td><?php echo $lastname; ?></td>
                                        <td><?php echo $phonenumber; ?></td>
                                        <td><a href="user_edit.php?edit=<?php echo $id; ?>" class="btn btn-secondary">
                                            <i class="fa fa-angle-double-right"></i> Edit
                                        </a></td>
                                        <td><a href="?delete=<?php echo $id; ?>" class="btn btn-secondary">
                                            <i class="fa fa-angle-double-right"></i> Delete
                                        </a></td>
                                    </tr>
                                    <?php
                                        $count++;
                                        } 
                                    ?>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <?php include "includes/paginator.php"; ?>

                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col">
                            <div class="card text-center bg-primary text-white mb-3">
                                <div class="card-body">
                                    <h3>Officers</h3>
                                    <h1 class="display-4">
                                        <i class="fa fa-pencil"></i> 6
                                    </h1>
                                    <a href="register.php" class="btn btn-outline-light btn-sm" >Add Officer</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>



    <footer id="main-footer" class="bg-dark fixed-bottom text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">Copyright &copy; 2020 WebMonitor </p>
                </div>
            </div>
        </div>
    </footer>



    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/trucks.js"><script>
</body>

</html>