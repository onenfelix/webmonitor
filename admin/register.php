<?php
    include "includes/header.php";
?>

<body>
    <?php
        include "includes/navigation.php";
    ?>

    <header id="main-header" class="py-2 bg-primary text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        <i class="fa fa-user"></i> Register an Officer</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- PROFILE EDIT -->

    <section id="profile">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="text-center">Fill the Form Below To Register A new Officer</h4>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="form-group">
                                    <label for="firstname">Username</label>
                                    <input type="text" name="username" class="form-control" value="onenfelix">
                                </div>
                                <div class="form-group">
                                    <label for="firstname">Firstname</label>
                                    <input type="text" name="firstname" class="form-control" value="Felix">
                                </div>
                                <div class="form-group">
                                    <label for="firstname">Lastname</label>
                                    <input type="text" name="lastname" class="form-control" value="Felix">
                                </div>
                                <div class="form-group">
                                    <label for="title">Email</label>
                                    <input type="email" class="form-control" value="onenfelix90@gmail.com">
                                </div>
                                <div class="form-group">
                                    <label for="phone">Telephone Number</label>
                                    <input type="text" name="phone" class="form-control" value="+256785450481">
                                </div>
                                <div class="form-group">
                                    <label for="file">Profile Image</label>
                                    <input type="file" id="file" class="form-control-file">
                                    <small id="fileHelp" class="form-text text-muted">Max 3mb size</small>
                                </div>
                                <div class="form-group">
                                    <label for="gender">Staff Status</label>
                                    <select id="gender" class="form-control">
                                    <option value="officer">Officer</option>
                                    <option value="admin">Inspector</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="password">Enter Temporary Password</label>
                                    <input name="password" type="password" class="form-control">
                                </div>

                                <button type="submit" class="btn mx-auto d-block btn-primary">Submit</button>

                            </form>
                        </div>
                    </div>

                </div>
                
            </div>
        </div>
    </section>

    <footer id="main-footer" class="bg-dark text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">Copyright &copy; 2020 WebMonitor </p>
                </div>
            </div>
        </div>
    </footer>


    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>