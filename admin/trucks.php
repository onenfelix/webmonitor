<?php
     include "includes/header.php";
     require('../db.php');
?>

<body>

    <?php
        include "includes/navigation.php";
    ?>

    <header id="main-header" class="py-2 bg-success text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        <i class="fa fa-folder"></i>Registered Trucks</h1>
                </div>
            </div>
        </div>
    </header>
    <!--ACTION-->
    <section id="action" class="py-4 mb-4 bg-light">
        <div class="container">
            <div class="row">
            <?php
            //check if file sent is an image else
            if(isset($_POST["submit"])) {
                $uploadOk = 1;
                if ($_FILES["truckImage"]["size"] > 300000) {
                echo " <div class='col-md-6'>

                    <div class='alert alert-success alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Failed!</strong> File is larger than the Required size
                    </div>

                    </div>";
                    $uploadOk = 0;
                }
            

                $imageFileType = strtolower(pathinfo($_FILES["truckImage"]["name"],PATHINFO_EXTENSION));
                
                // Allow certain file formats
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif") {
                    echo " <div class='col-md-6'>

                    <div class='alert alert-success alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                    <strong>Failed!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.
                    </div>

                    </div>";
                    $uploadOk = 0;
                }

                if($uploadOk == 1) {
                    echo "i still managed to reach here";
                    // check if the form has been submitted
                        $number_plate = $_POST['numberplate'];
                        $driver_name = $_POST['driverName'];
                        $driver_contact = $_POST['driverContact'];
                        $description = $_POST['description'];
                        $registration_date = date("Y/m/d");

                        $target_dir = "uploads/";
                        $target_file = $target_dir . basename($_FILES["truckImage"]["name"]);
                        move_uploaded_file($_FILES["truckImage"]["tmp_name"], $target_file);
                }

            }
            ?>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addTruckModal">
                        <i class="fa fa-plus"></i> Add New Truck
                    </a>
                </div>
                <div class="col-md-6 ml-auto">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="search">
                        <span class="input-group-btn">
                            <button class="btn btn-success">Search</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- CATEGORIES -->
    <?php
        //get current pagenumber
        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        //The formula for php pagination

        $no_of_records_per_page = 10;
        $offset = ($pageno-1) * $no_of_records_per_page; 
        // Get the number of total number of pages
        $total_pages_sql = "SELECT COUNT(*) FROM officers";
        $result = mysqli_query($con,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);
        //Constructing the SQL Query for pagination and retrieving information
        $sql = "SELECT numberplate,truck_image,driver_contact,driver_name,reg_date FROM truck_details LIMIT $offset, $no_of_records_per_page"; 
        $result = mysqli_query($con,$sql);
        // $rows = mysqli_fetch_array($result)
    ?>


    <!-- <section id="categories"> -->
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Monitored Trucks</h4>
                        </div>
                        <div class="table-responsive-lg">
                            <table class="table table-striped">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>#</th>
                                        <th>NumberPlate</th>
                                        <th>Truck Image</th>
                                        <th>Driver Name</th>
                                        <th>Driver Contact</th>
                                        <th>Date Added</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $count = 1;
                                    while($rows = mysqli_fetch_assoc($result)){
                                        $numberplate = $rows['numberplate'];
                                        $truckimage = $rows['truck_image'];
                                        $drivername = $rows['driver_name'];
                                        $drivercontact = $rows['driver_contact'];
                                        $dateadded = $rows['reg_date'];

                                    

                                ?>
                                    <tr>
                                        <td scope="row"><?php echo $count; ?></td>
                                        <td><?php echo $numberplate; ?></td>
                                        <td><img height="40" width="60" src="trucks/<?php echo $truckimage; ?>" alt="Truck Image"></td>
                                        <td><?php echo $drivername; ?></td>
                                        <td><?php echo $drivercontact; ?></td>
                                        <td><?php echo $dateadded; ?></td>
                                        <td><a href="truck_edit.php?edit=<?php echo $numberplate; ?>" class="btn btn-secondary">
                                                <i class="fa fa-angle-double-right"></i> Edit
                                            </a></td>
                                        <td><a href="?delete=<?php echo $numberplate; ?>" class="btn btn-secondary">
                                                <i class="fa fa-angle-double-right"></i> Delete
                                            </a></td>
                                    </tr>
                                    <?php
                                        $count++;
                                        } 
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- PAGINATOR FOR THE PAGE -->
                        <?php include "includes/paginator.php"; ?>
                        
                    </div>

                </div>
            </div>
        </div>
    <!-- </section> -->

    <footer id="main-footer" class="bg-dark text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">Copyright &copy; 2020 WebMonitor </p>
                </div>
            </div>
        </div>
    </footer>

    <!-- POST MODAL -->
    <div class="modal " id="addTruckModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">Add New Truck</h5>
                    <button class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <form action="trucks.php" method="post" enctype="multipart/form-data">
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="numberplate">NumberPlate</label>
                            <input type="text" name="numberplate" class="form-control" required>
                        </div>
                        
                        <div class="form-group">
                            <label for="file">Truck Image Upload</label>
                            <input type="file" name="truckImage" id="your-file"  class="form-control-file" required>
                            <small class="form-text text-muted">Max Size 3mb</small>
                        </div>
                        <div class="form-group">
                            <label for="driverName">Driver's Name</label>
                            <input type="text" name="driverName" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="driverContact">Driver's Contact</label>
                            <input type="text" name="driverContact" class="form-control"required>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea type="text" name="description" class="form-control"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                        
                        <input class="bg-primary" type="submit" name="submit" value="submit data" class="form-control">
                    </div>
                </form>
            </div>

        </div>
    </div>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/trucks.js"><script>
</body>

</html>