<nav class="navbar navbar-expand-lg navbar-dark bg-dark p-0">
        <div class="container">
            <a href="dashboard.php" class="navbar-brand">Web DUI Monitor</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item px-2">
                        <a href="dashboard.php" class="nav-link <?php if ($_SERVER['REQUEST_URI'] == '/webmonitor/admin/dashboard.php') { echo "active";  } ?> ">Violations</a>
                    </li>
                    <li class="nav-item px-2">
                        <a href="officers.php" class="nav-link <?php if ($_SERVER['REQUEST_URI'] == '/webmonitor/admin/officers.php') { echo "active";  } ?> ">Officers</a>
                    </li>
                    <li class="nav-item px-2">
                        <a href="trucks.php" class="nav-link <?php if ($_SERVER['REQUEST_URI'] == '/webmonitor/admin/trucks.php') { echo "active";  } ?>">Trucks</a>
                    </li>
                    
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown mr-3">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>Welcome Onen
                        </a>
                        <div class="dropdown-menu">
                            <a href="profile.html" class="dropdown-item">
                                <i class="fa fa-user-circle"></i>Profile
                            </a>
                            <a href="settings.html" class="dropdown-item">
                                <i class="fa fa-gear"></i>Settings
                            </a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="logout.php" class="nav-link">
                            <i class="fa fa-user-times"></i> Logout
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>