<?php
session_start(); // starts the session
    include "../db.php"; 
    //check if a user is logged in
    //if not sendhim to the login page
    if(!isset($_SESSION["username"]) && !isset($_SESSION["staff_status"]) ){
        $_SESSION['url'] = $_SERVER['REQUEST_URI'];
        header("Location:index.php");
        exit(); 
    }
    if(isset($_SESSION["username"]) && !isset($_SESSION["staff_status"]) ){
        $_SESSION['url'] = $_SERVER['REQUEST_URI'];
        header("Location:index.php");
        exit(); 
        }


?>


<!DOCTYPE html>
<html class="" lang="en">

<head>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <title>WebMonitor Admin Area</title>
</head>