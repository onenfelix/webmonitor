<?php
    include "includes/header.php";
?>

<body>
    <?php
        include "includes/navigation.php";
    ?>

    <header id="main-header" class="py-2 bg-primary text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        <i class="fa fa-user"></i> Edit Truck Details</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- PROFILE EDIT -->

    <section id="profile">
        <div class="container">
            <div class="row justify-content-center">
                <div class=" col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="text-center">Edit Form Below To Update Truck Details</h4>
                        </div>
                        <div class="card-body">
                            <form>
                            <div class="form-group">
                                <label for="numberplate">NumberPlate</label>
                                <input type="text" name="numberplate" class="form-control" required>
                            </div>
                            
                            <div class="form-group">
                                <label for="file">Truck Image Upload</label>
                                <input type="file" name="truckImage" id="your-file"  class="form-control-file" required>
                                <small class="form-text text-muted">Max Size 3mb</small>
                            </div>
                            <div class="form-group">
                                <label for="driverName">Driver's Name</label>
                                <input type="text" name="driverName" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="driverContact">Driver's Contact</label>
                                <input type="text" name="driverContact" class="form-control"required>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea type="text" name="description" class="form-control"></textarea>
                            </div>

                                <button type="submit" class="btn mx-auto d-block btn-primary">Submit</button>

                            </form>
                        </div>
                    </div>

                </div>
                
            </div>
        </div>
    </section>

    <footer id="main-footer" class="bg-dark text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">Copyright &copy; 2020 WebMonitor </p>
                </div>
            </div>
        </div>
    </footer>


    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>