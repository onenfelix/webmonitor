<?php
    include "includes/header.php";
    require('../db.php');
?>

<body>
    <?php
        include "includes/navigation.php";
    ?>

    <header id="main-header" class="py-2 bg-primary text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        <i class="fa fa-gear"></i>Dasboard</h1>
                </div>
            </div>
        </div>
    </header>
    <!--ACTION-->
    <section id="action" class="py-4 mb-2 bg-light ">
        <div class="container ml-auto">
            <div class="row ">
            <div class="col-md-9 ml-auto">
                <div class="row">
                <div class="col-md-3">
                    <div class="card text-center bg-primary text-white mb-3">
                        <div class="card-body">
                            <h3>All Records</h3>
                            <h1 class="display-4">
                                <i class="fa fa-pencil"></i> 6
                            </h1>
                            <a href="dashboard.php" class="btn btn-outline-light btn-sm">View</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card text-center bg-success text-white mb-3">
                            <div class="card-body">
                                <h3>Uncleared</h3>
                                <h1 class="display-4">
                                    <i class="fa fa-folder-open-o"></i> 4
                                </h1>
                                <a href="uncleared.php" class="btn btn-outline-light btn-sm">View</a>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card text-center bg-warning text-white mb-3">
                            <div class="card-body">
                                <h3>Cleared</h3>
                                <h1 class="display-4">
                                    <i class="fa fa-users"></i> 2
                                </h1>
                                <a href="cleared.php" class="btn btn-outline-light btn-sm">View</a>
                            </div>
                    </div>
                </div>
                </div>
             </div>
            </div>
        </div>
    </section>
    <!-- POST -->

    <?php
        //get current pagenumber
        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        //The formula for php pagination

        $no_of_records_per_page = 10;
        $offset = ($pageno-1) * $no_of_records_per_page; 
        // Get the number of total number of pages
        $total_pages_sql = "SELECT COUNT(*) FROM officers";
        $result = mysqli_query($con,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);
        //Constructing the SQL Query for pagination and retrieving information
        $sql = "SELECT numberplate,driver_name,violation_date FROM violations LIMIT $offset, $no_of_records_per_page"; 
        $result = mysqli_query($con,$sql);
        // $rows = mysqli_fetch_array($result)
    ?>

    <section id="posts">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h4>Latest Posts</h4>
                        </div>
                        <div class="table-responsive-lg">
                            <table class="table table-striped">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>#</th>
                                        <th>NumberPlate</th>
                                        <th>Driver Name</th>
                                        <th>Violation Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $count = 1;
                                    while($rows = mysqli_fetch_assoc($result)){
                                        $numberplate = $rows['numberplate'];
                                        $drivername = $rows['driver_name'];
                                        $dateadded = $rows['violation_date'];

                                    

                                ?>
                                 <tr>
                                        <td scope="row"><?php echo $count; ?></td>
                                        <td><?php echo $numberplate; ?></td>
                                        <td><?php echo $drivername; ?></td>
                                        <td><?php echo $dateadded; ?></td>
                                        <td><a href="truck_details.php?detail=<?php echo $numberplate; ?>" class="btn btn-secondary">
                                                <i class="fa fa-angle-double-right"></i> Details
                                            </a></td>
                                    </tr>
                                    <?php
                                        $count++;
                                        } 
                                    ?>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- PAGINATOR FOR THE PAGE -->
                    <?php include "includes/paginator.php"; ?>

                </div>
            </div>
        </div>
    </section>

    <footer id="main-footer" class="bg-dark text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">Copyright &copy; 2020 WebMonitor </p>
                </div>
            </div>
        </div>
    </footer>

    <!-- POST MODAL -->
    <div class="modal " id="addPostModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">Add Post</h5>
                    <button class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select type="text" class="form-control">
                                <option value="">Web development</option>
                                <option value="">Tech Gadgets</option>
                                <option value="">Business</option>
                                <option value="">Health and wellness</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="file">Image Upload</label>
                            <input type="file" class="form-control-file">
                            <small class="form-text text-muted">Max Size 3mb</small>
                        </div>
                        <div class="form-group">
                            <label for="body">Body</label>
                            <textarea type="text" class="form-control"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" data-dismiss="modal">Save Changes</button>
                </div>
            </div>

        </div>
    </div>

    <!-- CATEGORY MODAL -->
    <div class="modal " id="addCategoryModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-success text-white">
                        <h5 class="modal-title">Add Category</h5>
                        <button class="close" data-dismiss="modal">
                            <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button class="btn btn-success" data-dismiss="modal">Save Changes</button>
                    </div>
                </div>
    
            </div>
        </div>

        <!-- USER MODAL -->
    <div class="modal " id="addUserModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-warning text-white">
                        <h5 class="modal-title">Add User</h5>
                        <button class="close" data-dismiss="modal">
                            <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password">Confirm Password</label>
                                <input type="text" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button class="btn btn-warning" data-dismiss="modal">Save Changes</button>
                    </div>
                </div>
    
            </div>
        </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>