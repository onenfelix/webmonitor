<?php
    include "includes/header.php";
?>

<body>
    <?php
        include "includes/navigation.php";
    ?>

    <header id="main-header" class="py-2 bg-primary text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        <i class="fa fa-truck"></i> Truck Details</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- PROFILE EDIT -->

    <section id="profile">
        <div class="container">
            <div class="row justify-content-center">
                <div class=" col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="text-center">Truck Details</h4>
                        </div>
                        <div class="card-body">
                            <form>
                            <div class="form-group">
                                <label for="numberplate">NumberPlate</label>
                                <input type="text" value="UEA" name="numberplate" class="form-control" disabled>
                            </div>
                            
                            <div class="form-group">
                                <label for="file">Truck Image</label><br>
                                <img height="100" width="200" src="trucks/truck2.jpg" alt="Truck Image">
                                
                            </div>
                            <div class="form-group">
                                <label for="driverName">Driver's Name</label>
                                <input type="text" value="Onen Felix" name="driverName" class="form-control" disabled>
                            </div>
                            <div class="form-group">
                                <label for="driverContact">Driver's Contact</label>
                                <input type="text" value="+256785450481" name="driverContact" class="form-control" disabled>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea type="text" value="" name="description" class="form-control" disabled>
                                Truck carries 40kg and expected to run a 200km/hr
                                </textarea>
                            </div>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="card">
                            <div class="card-header">
                                <h4 class="text-center">Clearence Records</h4>
                            </div>
                            <div class="card-body">
                            <form>
                                <div class="form-group">
                                    <label for="numberplate"><strong>Cleared By:</strong> </label><br>
                                    <em><strong>officer =></strong></em><input type="text" value="Felix Onen" name="officersimage" class="form-control" disabled><br>
                                    <img height="150" width="150" src="trucks/truck2.jpg" alt="Truck Image"><br>
                                    <label for="numberplate"><strong>Violation Date:</strong> </label>
                                    <input type="text" value=" 22-3-2020 14:00" name="officersimage" class="form-control" disabled>

                                </div>
                            </form>

                            </div>
                    </div> 

                </div>
                
            </div>
        </div>
    </section>

    <footer id="main-footer" class="bg-dark text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">Copyright &copy; 2020 WebMonitor </p>
                </div>
            </div>
        </div>
    </footer>


    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>