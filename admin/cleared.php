<?php
    include "includes/header.php";
?>

<body>
    <?php
        include "includes/navigation.php";
    ?>

    <header id="main-header" class="py-2 bg-primary text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        <i class="fa fa-gear"></i>View cleared Violations</h1>
                </div>
            </div>
        </div>
    </header>
    <!--ACTION-->
            
    <!-- POST -->

    <section id="posts">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h4>Latest Posts</h4>
                        </div>
                        <div class="table-responsive-lg">
                            <table class="table table-striped">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>#</th>
                                        <th>NumberPlate</th>
                                        <th>Driver Name</th>
                                        <th>Violation Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row">1</td>
                                        <td>UEA</td>
                                        <td>OKELLO DENISH</td>
                                        <td>29/08/2020</td>
                                        <td><a href="truck_details.php" class="btn btn-secondary">
                                            <i class="fa fa-angle-double-right"></i> Details
                                        </a></td>
                                        
                                    </tr>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <nav class="ml-4 mt-2">
                            <ul class="pagination">
                                <li class="page-item disabled"><a href="#" class="page-link ">Previous</a></li>
                                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                                <li class="page-item"><a href="#" class="page-link">2</a></li>
                                <li class="page-item"><a href="#" class="page-link">3</a></li>
                                <li class="page-item"><a href="#" class="page-link">Next</a></li>
                            </ul>
                        </nav>

                </div>
            </div>
        </div>
    </section>

    <footer id="main-footer" class="bg-dark fixed-bottom text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">Copyright &copy; 2020 WebMonitor </p>
                </div>
            </div>
        </div>
    </footer>



    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>