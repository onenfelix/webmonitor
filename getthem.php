
<?php
    include "includes/header.php";
    require("db.php");
    //user id to be used for tracking purposes
    $officerid = $_SESSION['userid'];

?>

    <!-- CATEGORIES -->
    <?php
        //get current pagenumber
        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        //The formula for php pagination

        $no_of_records_per_page = 2;
        $offset = ($pageno-1) * $no_of_records_per_page; 
        // Get the number of total number of pages
        $total_pages_sql = "SELECT COUNT(*) FROM violations";
        $result = mysqli_query($con,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);
        //Constructing the SQL Query for pagination and retrieving information
        $sql = "SELECT * FROM violations WHERE cleared=0 LIMIT $offset, $no_of_records_per_page "; 
        $result = mysqli_query($con,$sql);
        // $rows = mysqli_fetch_array($result)
    ?>

<?php


while($row = mysqli_fetch_assoc($result)) {
        $numberplate = $row['numberplate'];
        $latitude = $row['latitude'];
        $longitude = $row['longitude'];



?>
<div class="row justify-content-center">
    <div class="col-md-6 mt-2 mb-2">
        <div class="alert alert-info text-center" role="alert">
        <div class="spinner-grow text-danger" role="status">
        <span class="sr-only">Loading...</span>
        </div>
        <h4 class="alert-heading"><strong> VIOLATION DETECTED : </strong></h4>
                <h5>Numberplate - <?php echo $numberplate; ?></h5>
            <p class="mb-0">Immediate Action Suggested</p>
            <!-- http://www.google.com/maps/place/lat,lng -->
            <a target="_blank" href="http://www.google.com/maps/place/<?php echo $latitude; ?>,<?php echo $longitude; ?>"  class="btn btn-danger text-white mr-2" type="button" >
            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
            take action...
            </a>
            <a href="details.php?numberplate=<?php echo $numberplate; ?>&officer=<?php echo $officerid; ?>" class="btn btn-outline-dark " type="button" >
            more info...
            </a>
        </div>
    </div>
</div>


<?php } ?>