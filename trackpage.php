<?php
    include "includes/header.php";
    //user id to be used for tracking purposes
    $officerid = $_SESSION['userid'];
    
?>

<body>
    <?php
            include "includes/navigation.php";
    ?>

    <header id="main-header" class="py-2 bg-info text-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col">
                    <h1 class="text-center">
                        REAL TIME VIOLATION MONITORING SECTION
                    </h1>
                </div>
            </div>
        </div>
    </header>
    <!--ACTION-->
    <!-- POST -->

    <section id="posts">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card" id="identify">
                     
                       
                    </div>

                </div>
            </div>
            <div class="row justify-content-center">
                <!-- PAGINATOR FOR THE PAGE -->
             <?php include "includes/mainpaginator.php"; ?>
            </div>
             
        </div>
    </section>

     

    <footer id="main-footer" class="bg-dark fixed-bottom text-white mt-5 p-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center">Copyright &copy; 2020 WebMonitor </p>
                </div>
            </div>
        </div>
    </footer>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/webmonitor.js"></script>
</body>

</html>